import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:test_app/screen_one/screen_one_desktop.dart';
import 'package:test_app/screen_one/screen_one_phone.dart';
import 'package:test_app/screen_one/screen_one_tablet.dart';

class ScreenOne extends StatelessWidget {
  const ScreenOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        switch (sizingInformation.deviceScreenType) {
          case DeviceScreenType.desktop:
            return const ScreenOneDesktop();
          case DeviceScreenType.tablet:
            return const ScreenOneTablet();
          default:
            return const ScreenOnePhone();
        }
      },
    );
  }
}
