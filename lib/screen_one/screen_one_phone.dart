import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../utils/image_res.dart';

class ScreenOnePhone extends StatefulWidget {
  const ScreenOnePhone({Key? key}) : super(key: key);

  @override
  State<ScreenOnePhone> createState() => _ScreenOnePhoneState();
}

class _ScreenOnePhoneState extends State<ScreenOnePhone> {
  final Map<int, Widget> logoWidgets = const <int, Widget>{
    0: Text('Arbeitnehmer'),
    1: Text('Arbeitgeber'),
    2: Text('Temporärbüro'),
  };
  int sharedValue = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xffE6FFFA),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 5,
                color: const Color(0xff3182CE),
              ),
              Container(
                width: double.infinity,
                height: kToolbarHeight,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey, //New
                      blurRadius: 5,
                      offset: Offset(0, -1),
                    )
                  ],
                ),
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(12),
                      bottomRight: Radius.circular(12),
                    ),
                    color: Colors.white,
                  ),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Text(
                        'Login',
                        style: TextStyle(
                          color: const Color(0xff319795),
                          fontSize: 12.sp,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const Center(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    'Deine Job\nwebsite',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 42),
                  ),
                ),
              ),
              Image.asset(
                ImageRes.backgroundImage,
                width: double.infinity,
                fit: BoxFit.fitWidth,
              ),
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12),
                    topLeft: Radius.circular(12),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey, //New
                      blurRadius: 5,
                      offset: Offset(0, -1),
                    )
                  ],
                ),
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(12),
                      topLeft: Radius.circular(12),
                    ),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              textStyle: const TextStyle(fontSize: 14),
                              primary: const Color(0xff3182CE),
                              onPrimary: Colors.white,
                              onSurface: Colors.grey,
                            ),
                            onPressed: () {},
                            child: const Text('Kostenlos Registrieren'),
                          ),
                        ),
                      ),
                      const SizedBox(height: 30),
                      SizedBox(
                        width: double.infinity,
                        child: CupertinoSegmentedControl<int>(
                          children: logoWidgets,
                          selectedColor: const Color(0xff81E6D9),
                          borderColor: const Color(0xffCBD5E0),
                          pressedColor:
                              const Color(0xff81E6D9).withOpacity(0.5),
                          onValueChanged: (int val) {
                            setState(() {
                              sharedValue = val;
                            });
                          },
                          groupValue: sharedValue,
                        ),
                      ),
                      const SizedBox(height: 30),
                      const Center(
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Text(
                            'Drei einfache Schritte zu deinem neuen Mitarbeiter',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 21),
                          ),
                        ),
                      ),
                      Image.asset(
                        sharedValue == 0
                            ? ImageRes.ss1
                            : (sharedValue == 1)
                                ? ImageRes.ss2
                                : ImageRes.ss3,
                        width: double.infinity,
                        fit: BoxFit.fitWidth,
                      ),
                      const SizedBox(height: 30),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
