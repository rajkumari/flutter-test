class ImageRes {
  static String backgroundImage = 'assets/undraw_agreement_aajr.png';
  static String backgroundImageWeb = 'assets/undraw_agreement_aajr_web.png';
  static const String ss1 = 'assets/ss1.png';
  static const String ss2 = 'assets/ss2.png';
  static const String ss3 = 'assets/ss3.png';
  static const String ss4 = 'assets/ss4.png';
  static const String ss6 = 'assets/ss6.png';
  static const String ss7 = 'assets/ss7.png';
}
