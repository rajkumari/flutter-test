import 'package:flutter/material.dart';
import 'package:test_app/screen_one/screen_one.dart';
import 'package:test_app/splash_screen/splash_screen.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/splash_screen/splash_screen_phone.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, _, __) {
        return MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const SplashScreen(),
        );
      },
    );
  }
}
