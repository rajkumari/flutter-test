import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:test_app/splash_screen/splash_screen_desktop.dart';
import 'package:test_app/splash_screen/splash_screen_phone.dart';
import 'package:test_app/splash_screen/splash_screen_tablet.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        switch (sizingInformation.deviceScreenType) {
          case DeviceScreenType.desktop:
            return const SplashScreenDesktop();
          case DeviceScreenType.tablet:
            return const SplashScreenTablet();
          default:
            return const SplashScreenPhone();
        }
      },
    );
  }
}
