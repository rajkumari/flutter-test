import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../screen_one/screen_one.dart';
import '../utils/navigation_helper.dart';

class SplashScreenTablet extends StatefulWidget {
  const SplashScreenTablet({Key? key}) : super(key: key);

  @override
  State<SplashScreenTablet> createState() => _SplashScreenTabletState();
}

class _SplashScreenTabletState extends State<SplashScreenTablet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color(0xff1E1C44),
        child: Center(
          child: InkWell(
            onTap: (() {
              NavigationHelper.navigate(context, const ScreenOne());
            }),
            child: Text(
              'Flutter Test',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30.sp, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
