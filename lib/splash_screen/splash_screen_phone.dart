import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../screen_one/screen_one.dart';
import '../utils/navigation_helper.dart';

class SplashScreenPhone extends StatefulWidget {
  const SplashScreenPhone({Key? key}) : super(key: key);

  @override
  State<SplashScreenPhone> createState() => _SplashScreenPhoneState();
}

class _SplashScreenPhoneState extends State<SplashScreenPhone> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color(0xff1E1C44),
        child: Center(
          child: InkWell(
            onTap: (() {
              NavigationHelper.navigate(context, const ScreenOne());
            }),
            child: Text(
              'Flutter Test',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30.sp, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
