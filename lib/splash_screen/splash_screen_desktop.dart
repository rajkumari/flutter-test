import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../screen_one/screen_one.dart';
import '../utils/navigation_helper.dart';

class SplashScreenDesktop extends StatefulWidget {
  const SplashScreenDesktop({Key? key}) : super(key: key);

  @override
  State<SplashScreenDesktop> createState() => _SplashScreenDesktopState();
}

class _SplashScreenDesktopState extends State<SplashScreenDesktop> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color(0xff1E1C44),
        child: Center(
          child: InkWell(
            onTap: (() {
              NavigationHelper.navigate(context, const ScreenOne());
            }),
            child: Text(
              'Flutter Test',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30.sp, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
